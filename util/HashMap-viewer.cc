#include <cstdlib>
#include <iostream>
#include <limits>
#include <regex>
#include <string>

namespace aisdi
{
    template <typename K, typename V>
    class HashMap;
}

template <typename K, typename V>
std::ostream& operator<<(std::ostream&, const aisdi::HashMap<K, V>&);

#define AISDI_ENABLE_OSTREAM
#include "HashMap.h"

template <typename K, typename V>
std::ostream& operator<<(std::ostream& os, const aisdi::HashMap<K, V>& tree)
{
    size_t current_idx = std::numeric_limits<size_t>::max();

    for (auto it = tree._data.begin(); it != tree._data.end(); ++it)
    {
        auto idx = tree._hashIndex((*it).first);

        if (idx != current_idx)
        {
            current_idx = idx;
            os << "\n[" << idx << "]";
        }

        os << " " << (*it).first << ",";
    }

    os << "\n";

    return os;
}

int main()
{
    aisdi::HashMap<int, std::string> map;

    std::regex rgx_insert(R"(\s*(i|insert)\s+([0-9]+)\s*)");
    std::regex rgx_delete(R"(\s*(d|delete)\s+([0-9]+)\s*)");
    std::regex rgx_print(R"(\s*(p|print)\s*)");
    std::regex rgx_quit(R"(\s*(q|quit)\s*)");
    std::smatch match;

    std::cout << "Available commands:" << "\n";
    std::cout << "    insert X" << "\n";
    std::cout << "    delete X" << "\n";
    std::cout << "    print" << "\n";
    std::cout << "    quit" << "\n";
    std::cout << "Single-letter short forms are available\n\n";

    auto warning = [](const char* msg)
    {
        std::cerr << msg << std::endl;
    };

    auto error = [](const char* msg)
    {
        std::cerr << msg << std::endl;
        std::exit(1);
    };

    while (true)
    {
        std::string line;
        std::cout << ">> " << std::flush;
        std::getline(std::cin, line);

        if (std::cin.eof()) break;
        else if (std::cin.bad()) error("I/O error");

        // Handle commands
        if (line.empty())
        {
            continue;
        }
        else if (std::regex_match(line, match, rgx_insert))
        {
            // Insert
            int i = std::stoi(match[2].str());

            if (map.find(i) == map.end())
                map.valueOf(i) = std::string{};
            else
                warning("Element already present");
        }
        else if (std::regex_match(line, match, rgx_delete))
        {
            // Delete
            int i = std::stoi(match[2].str());

            if (map.find(i) != map.end())
                map.remove(i);
            else
                warning("Element not present");
        }
        else if (std::regex_match(line, match, rgx_print))
        {
            // Print
            std::cout << map;
        }
        else if (std::regex_match(line, match, rgx_quit))
        {
            // Quit
            break;
        }
        else
        {
            warning("Unknown command");
        }
    }

    return 0;
}
