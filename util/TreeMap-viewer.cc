#include <cstdlib>
#include <functional>
#include <iostream>
#include <regex>
#include <string>
#include <vector>

namespace aisdi
{
    template <typename K, typename V>
    class TreeMap;
}

template <typename K, typename V>
std::ostream& operator<<(std::ostream&, const aisdi::TreeMap<K, V>&);

#define AISDI_ENABLE_OSTREAM
#include "TreeMap.h"

template <typename K, typename V>
std::ostream& operator<<(std::ostream& os, const aisdi::TreeMap<K, V>& tree)
{
    using node_pointer = typename aisdi::TreeMap<K, V>::node_pointer;

    std::vector<std::string> indent;

    std::function<void(node_pointer)> print = [&](node_pointer ptr)
    {
        static const std::string lh_line = u8" ├──";
        static const std::string rh_line = u8" └──";
        static const std::string v_space = "    ";
        static const std::string v_line = u8" │  ";
        static const std::string null_node = "*\n";

        if (ptr == nullptr) return;

        os << "<" << ptr->data.first << ">";
        if (ptr->balance < 0) os << "-";
        else if (ptr->balance > 0) os << "+";
        os << "\n";

        if (ptr->children[0] != nullptr || ptr->children[1] != nullptr)
        {
            for (auto& a : indent) os << a;
            os << lh_line;
            indent.push_back(v_line);

            if (ptr->children[0] != nullptr)
                print(ptr->children[0]);
            else
                os << null_node;

            indent.pop_back();

            for (auto& a : indent) os << a;
            os << rh_line;
            indent.push_back(v_space);

            if (ptr->children[1] != nullptr)
                print(ptr->children[1]);
            else
                os << null_node;

            indent.pop_back();
        }
    };

    print(tree._root);
    return os;
}

int main()
{
    aisdi::TreeMap<int, std::string> tree;

    std::regex rgx_insert(R"(\s*(i|insert)\s+([0-9]+)\s*)");
    std::regex rgx_delete(R"(\s*(d|delete)\s+([0-9]+)\s*)");
    std::regex rgx_print(R"(\s*(p|print)\s*)");
    std::regex rgx_quit(R"(\s*(q|quit)\s*)");
    std::smatch match;

    std::cout << "Available commands:" << "\n";
    std::cout << "    insert X" << "\n";
    std::cout << "    delete X" << "\n";
    std::cout << "    print" << "\n";
    std::cout << "    quit" << "\n";
    std::cout << "Single-letter short forms are available\n\n";

    auto warning = [](const char* msg)
    {
        std::cerr << msg << std::endl;
    };

    auto error = [](const char* msg)
    {
        std::cerr << msg << std::endl;
        std::exit(1);
    };

    while (true)
    {
        std::string line;
        std::cout << ">> " << std::flush;
        std::getline(std::cin, line);

        if (std::cin.eof()) break;
        else if (std::cin.bad()) error("I/O error");

        // Handle commands
        if (line.empty())
        {
            continue;
        }
        else if (std::regex_match(line, match, rgx_insert))
        {
            // Insert
            int i = std::stoi(match[2].str());

            if (tree.find(i) == tree.end())
                tree.valueOf(i) = std::string{};
            else
                warning("Element already present");
        }
        else if (std::regex_match(line, match, rgx_delete))
        {
            // Delete
            int i = std::stoi(match[2].str());

            if (tree.find(i) != tree.end())
                tree.remove(i);
            else
                warning("Element not present");
        }
        else if (std::regex_match(line, match, rgx_print))
        {
            // Print
            std::cout << tree;
        }
        else if (std::regex_match(line, match, rgx_quit))
        {
            // Quit
            break;
        }
        else
        {
            warning("Unknown command");
        }
    }

    return 0;
}
