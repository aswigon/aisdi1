function(aisdi_perf name)
    add_executable(${name} ${name}.cc)
endfunction()

aisdi_perf(Associative-benchmark)
aisdi_perf(Linear-benchmark)
