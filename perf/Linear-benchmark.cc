#include <cstdint>
#include <ctime>
#include <iomanip>
#include <iostream>
#include <vector>

#include "LinkedList.h"
#include "Vector.h"

namespace aisdi
{
    class Timer
    {
    public:
        void start() { _time = std::clock(); }
        void stop() { _time = std::clock() - _time; }
        double getTime() { return (_time * _factor); }

    private:
        std::clock_t _time = 0;
        constexpr static double _factor = 1.0 / static_cast<double>(CLOCKS_PER_SEC);
    };
}

int main()
{
    aisdi::Timer timer;
    std::vector<std::uint32_t> count{100, 1000, 10000};
    using tested_type = std::uint64_t;

    aisdi::LinkedList<tested_type> list;
    aisdi::Vector<tested_type> vector;

    std::cout << std::fixed << std::setprecision(4);
    std::cout << "\n" << std::setw(8) << "";
    std::cout << "\t" << "List\t" << "Vector" << std::endl;

    // Append test
    std::cout << std::endl << "append():\n";
    for (auto a : count)
    {
        std::cout << std::setw(7) << a << "x\t";

        timer.start();
        for (std::uint32_t i = 0; i < a; ++i) list.append(tested_type{});
        timer.stop();
        std::cout << timer.getTime() << "\t";

        timer.start();
        for (std::uint32_t i = 0; i < a; ++i) vector.append(tested_type{});
        timer.stop();
        std::cout << timer.getTime() << std::endl;
    }

    // Prepend test
    std::cout << std::endl << "prepend():\n";
    for (auto a : count)
    {
        std::cout << std::setw(7) << a << "x\t";

        timer.start();
        for (std::uint32_t i = 0; i < a; ++i) list.prepend(tested_type{});
        timer.stop();
        std::cout << timer.getTime() << "\t";

        timer.start();
        for (std::uint32_t i = 0; i < a; ++i) vector.prepend(tested_type{});
        timer.stop();
        std::cout << timer.getTime() << std::endl;
    }

    // PopFirst test
    std::cout << std::endl << "popFirst():\n";
    for (auto a : count)
    {
        std::cout << std::setw(7) << a << "x\t";

        timer.start();
        for (std::uint32_t i = 0; i < a; ++i) list.popFirst();
        timer.stop();
        std::cout << timer.getTime() << "\t";

        timer.start();
        for (std::uint32_t i = 0; i < a; ++i) vector.popFirst();
        timer.stop();
        std::cout << timer.getTime() << std::endl;
    }

    // Insert test
    std::cout << std::endl << "insert():\n";
    for (auto a : count)
    {
        std::cout << std::setw(7) << a << "x\t";

        aisdi::LinkedList<tested_type>::iterator li = list.begin() + a;
        aisdi::Vector<tested_type>::iterator lv = vector.begin() + a;

        timer.start();
        for (std::uint32_t i = 0; i < a; ++i) list.insert(li, tested_type{});
        timer.stop();
        std::cout << timer.getTime() << "\t";

        timer.start();
        for (std::uint32_t i = 0; i < a; ++i) vector.insert(lv, tested_type{});
        timer.stop();
        std::cout << timer.getTime() << std::endl;
    }

    // Erase test
    std::cout << std::endl << "erase():\n";
    for (auto a : count)
    {
        std::cout << std::setw(7) << a << "x\t";

        timer.start();
        for (std::uint32_t i = 0; i < a; ++i) list.erase(list.begin() + i);
        timer.stop();
        std::cout << timer.getTime() << "\t";

        timer.start();
        for (std::uint32_t i = 0; i < a; ++i) vector.erase(vector.begin() + i);
        timer.stop();
        std::cout << timer.getTime() << std::endl;
    }

    // PopLast test
    std::cout << std::endl << "popLast():\n";
    for (auto a : count)
    {
        std::cout << std::setw(7) << a << "x\t";

        timer.start();
        for (std::uint32_t i = 0; i < a; ++i) list.popLast();
        timer.stop();
        std::cout << timer.getTime() << "\t";

        timer.start();
        for (std::uint32_t i = 0; i < a; ++i) vector.popLast();
        timer.stop();
        std::cout << timer.getTime() << std::endl;
    }

    return 0;
}
