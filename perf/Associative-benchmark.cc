#include <cstdint>
#include <ctime>
#include <iomanip>
#include <iostream>
#include <limits>
#include <map>
#include <random>
#include <string>
#include <vector>

#include "HashMap.h"
#include "TreeMap.h"

namespace aisdi
{
    class Timer
    {
    public:
        void start() { _time = std::clock(); }
        void stop() { _time = std::clock() - _time; }
        double getTime() { return (_time * _factor); }

    private:
        std::clock_t _time = 0;
        constexpr static double _factor = 1.0 / static_cast<double>(CLOCKS_PER_SEC);
    };
}

int main()
{
    aisdi::Timer timer;
    std::vector<std::uint32_t> count{100, 1000, 10000};

    aisdi::HashMap<std::uint32_t, std::string> hash;
    aisdi::TreeMap<std::uint32_t, std::string> tree;

    std::cout << std::fixed << std::setprecision(4);
    std::cout << "\n" << std::setw(8) << "";
    std::cout << "\t" << "Hash\t" << "Tree" << std::endl;

    std::map<std::uint32_t, std::vector<std::uint32_t>> rand_values;

    // Insert test
    std::cout << std::endl << "valueOf():\n";
    for (auto a : count)
    {
        std::random_device rd;
        std::mt19937 gen(rd());

        for (std::uint32_t i = 0; i < (a + (a / 2)); ++i)
        {
            auto m = std::numeric_limits<std::uint32_t>::max() / 2;
            std::normal_distribution<double> d(m, m / 2);
            rand_values[a].push_back(static_cast<std::uint32_t>(d(gen)));
        }

        std::cout << std::setw(7) << a << "x\t";

        timer.start();
        for (auto k : rand_values[a]) hash.valueOf(k) = std::string{};
        timer.stop();
        std::cout << timer.getTime() << "\t";

        timer.start();
        for (auto k : rand_values[a]) tree.valueOf(k) = std::string{};
        timer.stop();
        std::cout << timer.getTime() << std::endl;
    }

    // Find test
    std::cout << std::endl << "find():\n";
    for (auto a : count)
    {
        std::cout << std::setw(7) << a << "x\t";

        timer.start();
        for (auto k : rand_values[a]) hash.find(k);
        timer.stop();
        std::cout << timer.getTime() << "\t";

        timer.start();
        for (auto k : rand_values[a]) tree.find(k);
        timer.stop();
        std::cout << timer.getTime() << std::endl;
    }

    // Remove test
    std::cout << std::endl << "remove():\n";
    for (auto a : count)
    {
        std::cout << std::setw(7) << a << "x\t";

        timer.start();
        for (auto k : rand_values[a]) hash.remove(k);
        timer.stop();
        std::cout << timer.getTime() << "\t";

        timer.start();
        for (auto k : rand_values[a]) tree.remove(k);
        timer.stop();
        std::cout << timer.getTime() << std::endl;
    }

    return 0;
}
