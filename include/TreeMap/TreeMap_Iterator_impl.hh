#ifndef AISDI_TREEMAP_ITERATOR_IMPL_HH
#define AISDI_TREEMAP_ITERATOR_IMPL_HH

#ifndef AISDI_TREEMAP_H
#error "Only include TreeMap.h"
#endif

#include "TreeMap.h"

template <typename K, typename V>
typename aisdi::TreeMap<K, V>::Iterator::pointer
aisdi::TreeMap<K, V>::Iterator::operator->() const
{
    return &this->operator*();
}

template <typename K, typename V>
typename aisdi::TreeMap<K, V>::Iterator::reference
aisdi::TreeMap<K, V>::Iterator::operator*() const
{
    return const_cast<reference>(ConstIterator::operator*());
}

template <typename K, typename V>
typename aisdi::TreeMap<K, V>::Iterator&
aisdi::TreeMap<K, V>::Iterator::operator++()
{
    ConstIterator::operator++();
    return *this;
}

template <typename K, typename V>
typename aisdi::TreeMap<K, V>::Iterator
aisdi::TreeMap<K, V>::Iterator::operator++(int)
{
    auto tmp = *this;
    ConstIterator::operator++();
    return tmp;
}

template <typename K, typename V>
typename aisdi::TreeMap<K, V>::Iterator&
aisdi::TreeMap<K, V>::Iterator::operator--()
{
    ConstIterator::operator--();
    return *this;
}

template <typename K, typename V>
typename aisdi::TreeMap<K, V>::Iterator
aisdi::TreeMap<K, V>::Iterator::operator--(int)
{
    auto tmp = *this;
    ConstIterator::operator--();
    return tmp;
}

#endif // !AISDI_TREEMAP_ITERATOR_IMPL_HH
