#ifndef AISDI_TREEMAP_IMPL_HH
#define AISDI_TREEMAP_IMPL_HH

#ifndef AISDI_TREEMAP_H
#error "Only include TreeMap.h"
#endif

#include "TreeMap.h"

template <typename K, typename V>
aisdi::TreeMap<K, V>::TreeMap()
{
    // Allocate guard
    _guard = _allocator.allocate(1);
    _guard->children[0] = _guard->children[1] = _guard->parent = nullptr;
    _guard->balance = 0;

    _root = nullptr;
    _size = 0;
}

template <typename K, typename V>
aisdi::TreeMap<K, V>::TreeMap(std::initializer_list<value_type> list) : TreeMap()
{
    for (auto& a : list) valueOf(a.first) = a.second;
}

template <typename K, typename V>
aisdi::TreeMap<K, V>::TreeMap(const TreeMap& other) : TreeMap()
{
    _allocator = other._allocator;
    //for (auto& a : other) valueOf(a.first) = a.second;
    // TODO: Balance tree

    std::function<void(node_pointer)> add = [&](node_pointer ptr)
    {
        if (ptr == nullptr) return;

        valueOf(ptr->data.first) = ptr->data.second;
        add(ptr->children[0]);
        add(ptr->children[1]);
    };

    add(other._root);
}

template <typename K, typename V>
aisdi::TreeMap<K, V>::TreeMap(TreeMap&& other) noexcept
{
    _allocator = std::move(other._allocator);
    _size = other._size;
    _guard = other._guard;
    _root = other._root;
    other._size = 0;
    other._guard = nullptr;
    other._root = nullptr;
}

template <typename K, typename V>
aisdi::TreeMap<K, V>::~TreeMap()
{
    _clear(_root);
    if (_guard != nullptr)
        _allocator.deallocate(_guard, 1);
}

template <typename K, typename V>
typename aisdi::TreeMap<K, V>&
aisdi::TreeMap<K, V>::operator=(const TreeMap& other)
{
    if (this == &other) return *this;

    _clear(_root);
    _size = 0;
    _guard->children[0] = _guard->children[1] = nullptr;
    _root = nullptr;

    _allocator = other._allocator;
    for (auto& a : other) valueOf(a.first) = a.second;
    return *this;
}

template <typename K, typename V>
typename aisdi::TreeMap<K, V>&
aisdi::TreeMap<K, V>::operator=(TreeMap&& other) noexcept
{
    assert(this != &other);

    _clear(_root);
    if (_guard != nullptr)
        _allocator.deallocate(_guard, 1);

    _allocator = std::move(other._allocator);
    _size = other._size;
    _guard = other._guard;
    _root = other._root;
    other._size = 0;
    other._guard = nullptr;
    other._root = nullptr;
    return *this;
}

template <typename K, typename V>
typename std::enable_if<std::is_standard_layout<typename aisdi::TreeMap<K, V>::node_type>::value,
    typename aisdi::TreeMap<K, V>::node_pointer>::type
aisdi::TreeMap<K, V>::_getNode(const_iterator pos)
{
    // Arithmetic is done on uint8_t* (size 1) instead of void* (forbidden)
    // This is a dirty hack, but is simplifies code a great deal
    union { const value_type* data; node_pointer node; std::uint8_t* raw; } ptr;
    ptr.data = &*pos;
    ptr.raw -= offsetof(node_type, data);
    return ptr.node;
}

template <typename K, typename V>
void
aisdi::TreeMap<K, V>::_rotateSingle(node_type** root, bool dir)
{
    assert(*root != nullptr);
    assert(dir + !dir == 1);

    node_pointer p, q, x;

    p = *root;
    q = p->children[!dir];
    x = q->children[dir];

    *root = q;
    q->parent = p->parent;
    p->parent = q;

    p->children[!dir] = x;
    q->children[dir] = p;
    x->parent = p;

    assert(*root == q);
    assert(q->parent != nullptr);
    assert(q->children[dir] == p);
    assert(p->parent == q);

    assert(p == p->children[dir]->parent);
    assert(p == p->children[!dir]->parent);
    assert(q == q->children[!dir]->parent);
    assert(q == q->children[dir]->parent);
}

template <typename K, typename V>
void
aisdi::TreeMap<K, V>::_rotateDouble(node_type** root, bool dir)
{
    assert(*root != nullptr);

    _rotateSingle(&(*root)->children[!dir], !dir);
    _rotateSingle(root, dir);
}

template <typename K, typename V>
void
aisdi::TreeMap<K, V>::_clear(node_pointer root)
{
    if (root != nullptr)
    {
        _clear(root->children[0]);
        _clear(root->children[1]);
        (root->data).~value_type();
        _allocator.deallocate(root, 1);
    }
}

template <typename K, typename V>
bool
aisdi::TreeMap<K, V>::isEmpty() const
{
    return (_size == 0);
}

template <typename K, typename V>
typename aisdi::TreeMap<K, V>::size_type
aisdi::TreeMap<K, V>::getSize() const
{
    return _size;
}

template <typename K, typename V>
const typename aisdi::TreeMap<K, V>::mapped_type&
aisdi::TreeMap<K, V>::valueOf(const key_type& key) const
{
    const_iterator it = find(key);
    if (it == cend()) throw std::out_of_range("No such key");

    assert(it->first == key);
    return it->second;
}

template <typename K, typename V>
typename aisdi::TreeMap<K, V>::mapped_type&
aisdi::TreeMap<K, V>::valueOf(const key_type& key)
{
    // Tree could be empty
    if (_root == nullptr)
    {
        assert(_size == 0);
        _root = _allocator.allocate(1);
        _root->children[0] = _root->children[1] = nullptr;
        _root->parent = _guard;
        _root->balance = 0;
        assert(_guard != nullptr);
        assert(_guard->parent == nullptr);

        _guard->children[0] = _root;
        assert(_guard->children[1] == nullptr);

        // Construct empty element
        new(&_root->data) value_type(key, mapped_type{});
        ++_size;
        return _root->data.second;
    }

    node_pointer pos = _root;
    while (true)
    {
        key_type k = pos->data.first;

        // Element for this key is already present
        if (key == k) return pos->data.second;

        bool dir = key > k;
        node_pointer tmp = pos->children[dir];

        // Make new node
        if (tmp == nullptr)
        {
            node_pointer new_node = _allocator.allocate(1);
            new_node->children[0] = new_node->children[1] = nullptr;
            new_node->parent = pos;
            new_node->balance = 0;
            pos->children[dir] = new_node;
            assert(new_node->parent->children[dir] == new_node);

            // Construct empty element
            new(&new_node->data) value_type(key, mapped_type{});
            ++_size;

            // TODO: Balance tree

            return new_node->data.second;
        }

        pos = tmp;
    }
}

template <typename K, typename V>
typename aisdi::TreeMap<K, V>::const_iterator
aisdi::TreeMap<K, V>::_find(const key_type& key) const
{
    node_pointer pos = _root;

    while (pos != nullptr)
    {
        key_type k = pos->data.first;
        if (key == k) return ConstIterator(pos);

        bool dir = key > k;
        pos = pos->children[dir];
    }

    return cend();
}

template <typename K, typename V>
typename aisdi::TreeMap<K, V>::const_iterator
aisdi::TreeMap<K, V>::find(const key_type& key) const
{
    return _find(key);
}

template <typename K, typename V>
typename aisdi::TreeMap<K, V>::iterator
aisdi::TreeMap<K, V>::find(const key_type& key)
{
    ConstIterator it = _find(key);
    return Iterator(it);
}

template <typename K, typename V>
void
aisdi::TreeMap<K, V>::remove(const key_type& key)
{
    remove(find(key));
}

template <typename K, typename V>
void
aisdi::TreeMap<K, V>::remove(const const_iterator& pos)
{
    if (pos == cend()) throw std::out_of_range("No such item");

    // TODO: Balance tree

    node_pointer start = _getNode(pos);
    node_pointer left = start->children[0];
    node_pointer right = start->children[1];

    assert(start->parent != nullptr);
    assert(start != _guard);

    bool top_dir = (start->parent->children[1] == start);
    assert(start->parent->children[top_dir] == start);

    if (left != nullptr && right != nullptr)
    {
        // Both children exist

        // TODO: Test this code path

        node_pointer tmp = start->children[top_dir];
        assert(tmp != nullptr);

        // Search for the minimum node in the right subtree or the maximum node in the left subtree
        while (true)
        {
            if (tmp->children[!top_dir] != nullptr)
                tmp = tmp->children[!top_dir];
            else
                break;
        }

        assert(tmp != nullptr);
        assert(tmp->children[!top_dir] == nullptr);

        // Move contents from tmp to start
        (start->data).~value_type();
        new(&start->data) value_type(std::move(tmp->data));

        // Recursive call to reduce code duplication
        remove(ConstIterator(tmp));
        return;
    }
    else
    {
        // At most one child
        assert(left == nullptr || right == nullptr);

        // Destruct contents
        (start->data).~value_type();

        // May be null
        node_pointer child = (left == nullptr) ? right : left;

        if (child != nullptr)
            child->parent = start->parent;

        // Update root if needed
        if (start == _root)
            _root = child;

        start->parent->children[top_dir] = child;

        _allocator.deallocate(start, 1);
        --_size;
        return;
    }
}

template <typename K, typename V>
const typename aisdi::TreeMap<K, V>::mapped_type&
aisdi::TreeMap<K, V>::operator[](const key_type& key) const
{
    return valueOf(key);
}

template <typename K, typename V>
typename aisdi::TreeMap<K, V>::mapped_type&
aisdi::TreeMap<K, V>::operator[](const key_type& key)
{
    return valueOf(key);
}

template <typename K, typename V>
bool
aisdi::TreeMap<K, V>::operator==(const TreeMap& other) const
{
    if (getSize() != other.getSize()) return false;

    ConstIterator a = cbegin();
    ConstIterator b = other.cbegin();

    while (a != cend() && b != other.cend())
    {
        if (*a++ != *b++) return false;
    }

    assert(a == cend());
    assert(b == other.cend());

    return true;
}

template <typename K, typename V>
bool
aisdi::TreeMap<K, V>::operator!=(const TreeMap& other) const
{
    return !(*this == other);
}

template <typename K, typename V>
typename aisdi::TreeMap<K, V>::iterator
aisdi::TreeMap<K, V>::begin()
{
    return Iterator(cbegin());
}

template <typename K, typename V>
typename aisdi::TreeMap<K, V>::iterator
aisdi::TreeMap<K, V>::end()
{
    return Iterator(_guard);
}

template <typename K, typename V>
typename aisdi::TreeMap<K, V>::const_iterator
aisdi::TreeMap<K, V>::cbegin() const
{
    if (_root == nullptr) return ConstIterator(_guard);

    // Move to the left bottom node
    node_pointer tmp = _root;
    while (tmp->children[0] != nullptr) tmp = tmp->children[0];
    return ConstIterator(tmp);
}

template <typename K, typename V>
typename aisdi::TreeMap<K, V>::const_iterator
aisdi::TreeMap<K, V>::cend() const
{
    return ConstIterator(_guard);
}

template <typename K, typename V>
typename aisdi::TreeMap<K, V>::const_iterator
aisdi::TreeMap<K, V>::begin() const
{
    return cbegin();
}

template <typename K, typename V>
typename aisdi::TreeMap<K, V>::const_iterator
aisdi::TreeMap<K, V>::end() const
{
    return cend();
}

#endif // !AISDI_TREEMAP_IMPL_HH
