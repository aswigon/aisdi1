#ifndef AISDI_TREEMAP_CONSTITERATOR_IMPL_HH
#define AISDI_TREEMAP_CONSTITERATOR_IMPL_HH

#ifndef AISDI_TREEMAP_H
#error "Only include TreeMap.h"
#endif

#include "TreeMap.h"

template <typename K, typename V>
aisdi::TreeMap<K, V>::ConstIterator::ConstIterator(node_pointer pos)
{
    _pos = pos;
}

template <typename K, typename V>
typename aisdi::TreeMap<K, V>::ConstIterator::pointer
aisdi::TreeMap<K, V>::ConstIterator::operator->() const
{
    return &this->operator*();
}

template <typename K, typename V>
typename aisdi::TreeMap<K, V>::ConstIterator::reference
aisdi::TreeMap<K, V>::ConstIterator::operator*() const
{
    if (_pos->parent == nullptr) throw std::out_of_range("Iterator out of range");
    return _pos->data;
}

template <typename K, typename V>
typename aisdi::TreeMap<K, V>::ConstIterator&
aisdi::TreeMap<K, V>::ConstIterator::_seek(bool dir)
{
    // True (1) performs operator++, false (0) performs operator--

    if (_pos->children[dir] != nullptr)
    {
        // Move down once in seeking direction
        _pos = _pos->children[dir];

        // Move down in the opposite direction
        while (_pos->children[!dir] != nullptr)
            _pos = _pos->children[!dir];

        return *this;
    }
    else
    {
        if (_pos->parent == nullptr) throw std::out_of_range("Iterator out of range");

        // Move up in the opposite direction
        while (_pos->parent->children[dir] == _pos)
            _pos = _pos->parent;

        // Move up once in seeking direction
        assert(_pos->parent->children[!dir] == _pos);
        _pos = _pos->parent;

        return *this;
    }
}

template <typename K, typename V>
typename aisdi::TreeMap<K, V>::ConstIterator&
aisdi::TreeMap<K, V>::ConstIterator::operator++()
{
    return _seek(1);
}

template <typename K, typename V>
typename aisdi::TreeMap<K, V>::ConstIterator
aisdi::TreeMap<K, V>::ConstIterator::operator++(int)
{
    auto tmp = *this;
    operator++();
    return tmp;
}

template <typename K, typename V>
typename aisdi::TreeMap<K, V>::ConstIterator&
aisdi::TreeMap<K, V>::ConstIterator::operator--()
{
    return _seek(0);
}

template <typename K, typename V>
typename aisdi::TreeMap<K, V>::ConstIterator
aisdi::TreeMap<K, V>::ConstIterator::operator--(int)
{
    auto tmp = *this;
    operator--();
    return tmp;
}

template <typename K, typename V>
bool
aisdi::TreeMap<K, V>::ConstIterator::operator==(const ConstIterator& other) const
{
    return (_pos == other._pos);
}

template <typename K, typename V>
bool
aisdi::TreeMap<K, V>::ConstIterator::operator!=(const ConstIterator& other) const
{
    return !(*this == other);
}

#endif // !AISDI_TREEMAP_CONSTITERATOR_IMPL_HH
