#ifndef AISDI_TREEMAP_H
#define AISDI_TREEMAP_H

#include <cassert>
#include <cstddef>
#include <cstdint>
#include <functional>
#include <initializer_list>
#include <iterator>
#include <memory>
#include <stdexcept>
#include <type_traits>
#include <utility>

namespace aisdi
{
    template <typename K, typename V>
    class TreeMap
    {
    public:
        class ConstIterator;
        class Iterator;

        using key_type = K;
        using mapped_type = V;
        using value_type = std::pair<const key_type, mapped_type>;
        using size_type = std::size_t;
        using reference = value_type&;
        using const_reference = const value_type&;
        using iterator = Iterator;
        using const_iterator = ConstIterator;
        using node_type = struct Node { Node* parent; Node* children[2]; value_type data; std::int8_t balance; };
        using node_pointer = Node*;

        TreeMap();
        TreeMap(std::initializer_list<value_type>);
        TreeMap(const TreeMap&);
        TreeMap(TreeMap&&) noexcept;
        ~TreeMap();

        TreeMap& operator=(const TreeMap&);
        TreeMap& operator=(TreeMap&&) noexcept;

        bool isEmpty() const;
        size_type getSize() const;
        const mapped_type& valueOf(const key_type&) const;
        mapped_type& valueOf(const key_type&);
        const_iterator find(const key_type&) const;
        iterator find(const key_type&);
        void remove(const key_type&);
        void remove(const const_iterator&);

        const mapped_type& operator[](const key_type&) const;
        mapped_type& operator[](const key_type&);
        bool operator==(const TreeMap&) const;
        bool operator!=(const TreeMap& other) const;

        iterator begin();
        iterator end();
        const_iterator cbegin() const;
        const_iterator cend() const;
        const_iterator begin() const;
        const_iterator end() const;

    private:
        std::allocator<Node> _allocator;
        size_type _size;
        node_pointer _root;
        node_pointer _guard;

        static typename std::enable_if<std::is_standard_layout<node_type>::value, node_pointer>::type
        _getNode(const_iterator);

        void _rotateSingle(node_type**, bool);
        void _rotateDouble(node_type**, bool);
        void _clear(node_pointer);
        const_iterator _find(const key_type&) const;

#if defined(DEBUG) && defined(AISDI_ENABLE_OSTREAM)
        friend std::ostream& operator<<<K, V>(std::ostream&, const TreeMap<K, V>&);
#endif
    };

    template <typename K, typename V>
    class TreeMap<K, V>::ConstIterator
    {
    public:
        using reference = typename TreeMap::const_reference;
        using iterator_category = std::bidirectional_iterator_tag;
        using value_type = typename TreeMap::value_type;
        using pointer = const typename TreeMap::value_type*;

        ConstIterator() = delete;
        ConstIterator(node_pointer);
        ConstIterator(const ConstIterator&) = default;

        pointer operator->() const;
        reference operator*() const;
        ConstIterator& operator++();
        ConstIterator operator++(int);
        ConstIterator& operator--();
        ConstIterator operator--(int);
        bool operator==(const ConstIterator& other) const;
        bool operator!=(const ConstIterator& other) const;

    private:
        node_pointer _pos;

        ConstIterator& _seek(bool dir);
    };

    template <typename K, typename V>
    class TreeMap<K, V>::Iterator : public TreeMap<K, V>::ConstIterator
    {
    public:
        using reference = typename TreeMap::reference;
        using pointer = typename TreeMap::value_type*;

        Iterator() = delete;
        Iterator(node_pointer ptr) : ConstIterator(ptr) {}
        Iterator(const ConstIterator& other) : ConstIterator(other) {}

        pointer operator->() const;
        reference operator*() const;
        Iterator& operator++();
        Iterator operator++(int);
        Iterator& operator--();
        Iterator operator--(int);
    };
}

#include "TreeMap/TreeMap_impl.hh"
#include "TreeMap/TreeMap_ConstIterator_impl.hh"
#include "TreeMap/TreeMap_Iterator_impl.hh"

#endif // !AISDI_TREEMAP_H
