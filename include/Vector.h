#ifndef AISDI_VECTOR_H
#define AISDI_VECTOR_H

#include <algorithm>
#include <cassert>
#include <cstddef>
#include <iterator>
#include <initializer_list>
#include <limits>
#include <memory>
#include <stdexcept>
#include <utility>

namespace aisdi
{
    template <typename T>
    class Vector
    {
    public:
        class ConstIterator;
        class Iterator;

        using difference_type = std::ptrdiff_t;
        using size_type = std::size_t;
        using value_type = T;
        using pointer = T*;
        using reference = T&;
        using const_pointer = const T*;
        using const_reference = const T&;
        using iterator = Iterator;
        using const_iterator = ConstIterator;

        Vector() : Vector(_default_nonzero_count) {}
        Vector(size_type);
        Vector(std::initializer_list<T>);
        Vector(const Vector&);
        Vector(Vector&&) noexcept;
        ~Vector();

        Vector& operator=(const Vector& other);
        Vector& operator=(Vector&& other) noexcept;

        bool isEmpty() const;
        size_type getSize() const;
        void append(const_reference);
        void prepend(const_reference);
        void insert(const const_iterator&, const_reference);
        value_type popFirst();
        value_type popLast();
        void erase(const const_iterator&);
        void erase(const const_iterator&, const const_iterator&);

        iterator begin();
        iterator end();
        const_iterator cbegin() const;
        const_iterator cend() const;
        const_iterator begin() const;
        const_iterator end() const;

    private:
        static const size_type _default_nonzero_count = 2;
        constexpr static size_type _max_count = std::numeric_limits<size_type>::max() / sizeof(T);
        std::allocator<T> _allocator;
        size_type _size = 0;
        size_type _capacity = 0;
        value_type* _buffer = nullptr;

        void _grow();
    };

    template <typename T>
    class Vector<T>::ConstIterator
    {
    public:
        using iterator_category = std::bidirectional_iterator_tag;
        using value_type = typename Vector::value_type;
        using difference_type = typename Vector::difference_type;
        using pointer = typename Vector::const_pointer;
        using reference = typename Vector::const_reference;

        ConstIterator() = delete;
        ConstIterator(const_pointer const*, const size_type*, size_type);
        ConstIterator(const ConstIterator&) = default;

        reference operator*() const;
        ConstIterator& operator++();
        ConstIterator operator++(int);
        ConstIterator& operator--();
        ConstIterator operator--(int);
        ConstIterator operator+(difference_type d) const;
        ConstIterator operator-(difference_type d) const;
        bool operator==(const ConstIterator& other) const;
        bool operator!=(const ConstIterator& other) const;

    private:
        const size_type* _size;
        const_pointer const* _buf;
        size_type _pos;
    };

    template <typename T>
    class Vector<T>::Iterator : public Vector<T>::ConstIterator
    {
    public:
        using pointer = typename Vector::pointer;
        using reference = typename Vector::reference;

        Iterator() = delete;
        Iterator(const_pointer const* b, const size_type* s, size_type d) : ConstIterator(b, s, d) {}
        Iterator(const ConstIterator& other) : ConstIterator(other) {}

        reference operator*() const;
        Iterator& operator++();
        Iterator operator++(int);
        Iterator& operator--();
        Iterator operator--(int);
        Iterator operator+(difference_type) const;
        Iterator operator-(difference_type) const;
    };
}

#include "Vector/Vector_impl.hh"
#include "Vector/Vector_ConstIterator_impl.hh"
#include "Vector/Vector_Iterator_impl.hh"

#endif // !AISDI_VECTOR_H
