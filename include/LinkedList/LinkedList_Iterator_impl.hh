#ifndef AISDI_LINKEDLIST_ITERATOR_IMPL_HH
#define AISDI_LINKEDLIST_ITERATOR_IMPL_HH

#ifndef AISDI_LINKEDLIST_H
#error "Only include LinkedList.h"
#endif

#include "LinkedList.h"

template <typename T>
typename aisdi::LinkedList<T>::reference
aisdi::LinkedList<T>::Iterator::operator*() const
{
    return const_cast<reference>(ConstIterator::operator*());
}

template <typename T>
typename aisdi::LinkedList<T>::Iterator&
aisdi::LinkedList<T>::Iterator::operator++()
{
    ConstIterator::operator++();
    return *this;
}

template <typename T>
typename aisdi::LinkedList<T>::Iterator
aisdi::LinkedList<T>::Iterator::operator++(int)
{
    auto tmp = *this;
    ConstIterator::operator++();
    return tmp;
}

template <typename T>
typename aisdi::LinkedList<T>::Iterator&
aisdi::LinkedList<T>::Iterator::operator--()
{
    ConstIterator::operator--();
    return *this;
}

template <typename T>
typename aisdi::LinkedList<T>::Iterator
aisdi::LinkedList<T>::Iterator::operator--(int)
{
    auto tmp = *this;
    ConstIterator::operator--();
    return tmp;
}

template <typename T>
typename aisdi::LinkedList<T>::Iterator
aisdi::LinkedList<T>::Iterator::operator+(difference_type d) const
{
    return ConstIterator::operator+(d);
}

template <typename T>
typename aisdi::LinkedList<T>::Iterator
aisdi::LinkedList<T>::Iterator::operator-(difference_type d) const
{
    return ConstIterator::operator-(d);
}

#endif // !AISDI_LINKEDLIST_ITERATOR_IMPL_HH
