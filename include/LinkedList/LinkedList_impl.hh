#ifndef AISDI_LINKEDLIST_IMPL_HH
#define AISDI_LINKEDLIST_IMPL_HH

#ifndef AISDI_LINKEDLIST_H
#error "Only include LinkedList.h"
#endif

#include "LinkedList.h"

// Guarantee for HashMap: _tail/end() does not change when inserting new elements

template <typename T>
aisdi::LinkedList<T>::LinkedList()
{
    // Allocate guard
    node_pointer tmp = _allocator.allocate(1);
    tmp->next = nullptr;
    tmp->prev = nullptr;
    _head = _tail = tmp;
    _size = 0;
}

template <typename T>
aisdi::LinkedList<T>::LinkedList(std::initializer_list<T> list) : LinkedList()
{
    for (auto& a : list) append(a);
}

template <typename T>
aisdi::LinkedList<T>::LinkedList(const LinkedList& other) : LinkedList()
{
    _allocator = other._allocator;
    for (auto& a : other) append(a);
}

template <typename T>
aisdi::LinkedList<T>::LinkedList(LinkedList&& other) noexcept
{
    _allocator = std::move(other._allocator);
    _size = other._size;
    _head = other._head;
    _tail = other._tail;
    other._size = 0;
    other._head = nullptr;
    other._tail = nullptr;
}

template <typename T>
aisdi::LinkedList<T>::~LinkedList()
{
    erase(begin(), end());
    if (_tail != nullptr) _allocator.deallocate(_tail, 1);
}

template <typename T>
typename aisdi::LinkedList<T>&
aisdi::LinkedList<T>::operator=(const LinkedList& other)
{
    if (this == &other) return *this;

    erase(begin(), end());
    _allocator = other._allocator;
    for (auto& a : other) append(a);
    return *this;
}

template <typename T>
typename aisdi::LinkedList<T>&
aisdi::LinkedList<T>::operator=(LinkedList&& other) noexcept
{
    assert(this != &other);

    erase(begin(), end());
    _allocator.deallocate(_tail, 1);
    _allocator = std::move(other._allocator);
    _size = other._size;
    _head = other._head;
    _tail = other._tail;
    other._size = 0;
    other._head = nullptr;
    other._tail = nullptr;
    return *this;
}

template <typename T>
typename std::enable_if<std::is_standard_layout<typename aisdi::LinkedList<T>::node_type>::value,
    typename aisdi::LinkedList<T>::node_pointer>::type
aisdi::LinkedList<T>::_getNode(const_iterator pos)
{
    // Arithmetic is done on uint8_t* (size 1) instead of void* (forbidden)
    // This is a dirty hack, but is simplifies code a great deal
    union { const_pointer data; node_pointer node; std::uint8_t* raw; } ptr;
    ptr.data = &*pos;
    ptr.raw -= offsetof(node_type, data);
    return ptr.node;
}

template <typename T>
bool
aisdi::LinkedList<T>::isEmpty() const
{
    return (_size == 0);
}

template <typename T>
typename aisdi::LinkedList<T>::size_type
aisdi::LinkedList<T>::getSize() const
{
    return _size;
}

template <typename T>
void
aisdi::LinkedList<T>::append(const_reference item)
{
    if (_size != 0)
    {
        // Allocate new node
        node_pointer tmp = _allocator.allocate(1);
        tmp->next = _tail;
        tmp->prev = _tail->prev;

        // Place new object
        _tail->prev->next = tmp;
        _tail->prev = tmp;
        new(&tmp->data) T(item);
        ++_size;
    }
    else
    {
        prepend(item);
    }
}

template <typename T>
void
aisdi::LinkedList<T>::prepend(const_reference item)
{
    // Allocate new node
    node_pointer tmp = _allocator.allocate(1);
    tmp->next = _head;
    tmp->prev = nullptr;

    // Place new object
    _head->prev = tmp;
    new(&tmp->data) T(item);
    _head = tmp;
    ++_size;
}

template <typename T>
void
aisdi::LinkedList<T>::insert(const const_iterator& pos, const_reference item)
{
    if (pos == begin())
    {
        prepend(item);
    }
    else if (pos == end())
    {
        append(item);
    }
    else
    {
        node_pointer node_ptr = _getNode(pos);
        node_pointer new_node = _allocator.allocate(1);

        // Link new node
        new_node->next = node_ptr;
        new_node->prev = node_ptr->prev;
        node_ptr->prev->next = new_node;
        node_ptr->prev = new_node;
        new(&new_node->data) T(item);
        ++_size;
    }
}

template <typename T>
typename aisdi::LinkedList<T>::value_type
aisdi::LinkedList<T>::popFirst()
{
    if (_size == 0) throw std::logic_error("Cannot pop from empty collection");

    value_type tmp = std::move(_head->data);
    node_pointer new_head = _head->next;
    new_head->prev = nullptr;
    _allocator.deallocate(_head, 1);
    _head = new_head;
    --_size;

    return std::move(tmp);
}

template <typename T>
typename aisdi::LinkedList<T>::value_type
aisdi::LinkedList<T>::popLast()
{
    if (_size == 0) throw std::logic_error("Cannot pop from empty collection");

    node_pointer last = _tail->prev;
    value_type tmp = std::move(last->data);
    _tail->prev = last->prev;
    // Last item might also be the head
    if (last->prev == nullptr) _head = _tail;
    else last->prev->next = _tail;
    _allocator.deallocate(last, 1);
    --_size;

    return std::move(tmp);
}

template <typename T>
void
aisdi::LinkedList<T>::erase(const const_iterator& pos)
{
    if (pos == end()) throw std::out_of_range("Cannot erase guard");

    node_pointer node_ptr = _getNode(pos);
    const_pointer data_ptr = &node_ptr->data;
    data_ptr->~T();

    if (node_ptr->prev == nullptr)
        _head = node_ptr->next;
    else
        node_ptr->prev->next = node_ptr->next;

    node_ptr->next->prev = node_ptr->prev;
    _allocator.deallocate(node_ptr, 1);
    --_size;
}

template <typename T>
void
aisdi::LinkedList<T>::erase(const const_iterator& left, const const_iterator& right)
{
    if (left == right) return;

    node_pointer l_node_ptr = _getNode(left);
    node_pointer r_node_ptr;

    if (right == cend())
        r_node_ptr = _tail;
    else
        r_node_ptr = _getNode(right);

    node_pointer l_saved = nullptr;
    if (left != cbegin()) l_saved = l_node_ptr->prev;
    node_pointer r_saved = r_node_ptr;

    // Iterate over nodes to be freed
    node_pointer pos = l_node_ptr;
    while (pos != r_saved)
    {
        node_pointer tmp = pos->next;
        (&pos->data)->~T();
        _allocator.deallocate(pos, 1);
        --_size;
        pos = tmp;
    }

    // Link ends
    if (l_saved == nullptr)
        _head = r_saved;
    else
        l_saved->next = r_saved;

    r_saved->prev = l_saved;
}

template <typename T>
typename aisdi::LinkedList<T>::iterator
aisdi::LinkedList<T>::begin()
{
    return Iterator(_head);
}

template <typename T>
typename aisdi::LinkedList<T>::iterator
aisdi::LinkedList<T>::end()
{
    return Iterator(_tail);
}

template <typename T>
typename aisdi::LinkedList<T>::const_iterator
aisdi::LinkedList<T>::cbegin() const
{
    return ConstIterator(_head);
}

template <typename T>
typename aisdi::LinkedList<T>::const_iterator
aisdi::LinkedList<T>::cend() const
{
    return ConstIterator(_tail);
}

template <typename T>
typename aisdi::LinkedList<T>::const_iterator
aisdi::LinkedList<T>::begin() const
{
    return cbegin();
}

template <typename T>
typename aisdi::LinkedList<T>::const_iterator
aisdi::LinkedList<T>::end() const
{
    return cend();
}

#endif // !AISDI_LINKEDLIST_IMPL_HH
