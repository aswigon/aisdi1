#ifndef AISDI_LINKEDLIST_CONSTITERATOR_IMPL_HH
#define AISDI_LINKEDLIST_CONSTITERATOR_IMPL_HH

#ifndef AISDI_LINKEDLIST_H
#error "Only include LinkedList.h"
#endif

#include "LinkedList.h"

template <typename T>
aisdi::LinkedList<T>::ConstIterator::ConstIterator(node_pointer pos)
{
    _pos = pos;
}

template <typename T>
typename aisdi::LinkedList<T>::const_reference
aisdi::LinkedList<T>::ConstIterator::operator*() const
{
    if (_pos->next == nullptr) throw std::out_of_range("Iterator out of range");
    return _pos->data;
}

template <typename T>
typename aisdi::LinkedList<T>::ConstIterator&
aisdi::LinkedList<T>::ConstIterator::operator++()
{
    if (_pos->next == nullptr) throw std::out_of_range("Iterator out of range");
    _pos = _pos->next;
    return *this;
}

template <typename T>
typename aisdi::LinkedList<T>::ConstIterator
aisdi::LinkedList<T>::ConstIterator::operator++(int)
{
    auto tmp = *this;
    operator++();
    return tmp;
}

template <typename T>
typename aisdi::LinkedList<T>::ConstIterator&
aisdi::LinkedList<T>::ConstIterator::operator--()
{
    if (_pos->prev == nullptr) throw std::out_of_range("Iterator out of range");
    _pos = _pos->prev;
    return *this;
}

template <typename T>
typename aisdi::LinkedList<T>::ConstIterator
aisdi::LinkedList<T>::ConstIterator::operator--(int)
{
    auto tmp = *this;
    operator--();
    return tmp;
}

template <typename T>
typename aisdi::LinkedList<T>::ConstIterator
aisdi::LinkedList<T>::ConstIterator::operator+(difference_type d) const
{
    ConstIterator tmp(_pos);

    // Move one-by-one
    if (d > 0) while (d-- > 0) ++tmp;
    else if (d < 0) while (d++ < 0) --tmp;
    return tmp;
}

template <typename T>
typename aisdi::LinkedList<T>::ConstIterator
aisdi::LinkedList<T>::ConstIterator::operator-(difference_type d) const
{
    return operator+(-d);
}

template <typename T>
bool
aisdi::LinkedList<T>::ConstIterator::operator==(const ConstIterator& other) const
{
    return (_pos == other._pos);
}

template <typename T>
bool
aisdi::LinkedList<T>::ConstIterator::operator!=(const ConstIterator& other) const
{
    return !(*this == other);
}

#endif // !AISDI_LINKEDLIST_CONSTITERATOR_IMPL_HH
