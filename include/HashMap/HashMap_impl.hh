#ifndef AISDI_HASHMAP_IMPL_HH
#define AISDI_HASHMAP_IMPL_HH

#ifndef AISDI_HASHMAP_H
#error "Only include HashMap.h"
#endif

#include "HashMap.h"

template <typename K, typename V>
aisdi::HashMap<K, V>::HashMap(size_type size) : _index(_ceilingPowerOf2(size))
{
    size_type power = _ceilingPowerOf2(size);

    _bucket_count = power;
    _bucket_mask = power - 1;

    // Initialize index
    iterator i = end();
    for (size_t s = 0; s < power; ++s) _index.append(i);
    // Note: It is guaranteed by LinkedList that end() remains valid after insertions
    assert(begin() == end());
}

template <typename K, typename V>
aisdi::HashMap<K, V>::HashMap(std::initializer_list<value_type> list) : HashMap(list.size())
{
    for (auto& a : list) valueOf(a.first) = a.second;
}

template <typename K, typename V>
aisdi::HashMap<K, V>::HashMap(const HashMap& other) : HashMap(other.getSize())
{
    for (auto& a : other) valueOf(a.first) = a.second;
}

template <typename K, typename V>
aisdi::HashMap<K, V>::HashMap(HashMap&& other) noexcept
{
    assert(other._bucket_count == other._index.getSize());
    _bucket_count = other._bucket_count;
    _bucket_mask = other._bucket_mask;

    assert(_isPowerOf2(_bucket_count));
    assert(_bucket_mask + 1 == _bucket_count);

    _index = std::move(other._index);
    _data = std::move(other._data);
}

template <typename K, typename V>
typename aisdi::HashMap<K, V>&
aisdi::HashMap<K, V>::operator=(const HashMap& other)
{
    if (this == &other) return *this;

    _bucket_count = _ceilingPowerOf2(other.getSize());
    _bucket_mask = _bucket_count - 1;
    assert(_isPowerOf2(_bucket_count));
    assert(_bucket_mask + 1 == _bucket_count);

    // Erase old data
    _data.erase(_data.begin(), _data.end());
    assert(getSize() == 0);

    // Initialize new index
    Vector<iterator> new_index(_bucket_count);
    iterator i = end();
    for (size_t s = 0; s < _bucket_count; ++s) new_index.append(i);

    std::swap(_index, new_index);
    for (auto& a : other) valueOf(a.first) = a.second;

    return *this;
}

template <typename K, typename V>
typename aisdi::HashMap<K, V>&
aisdi::HashMap<K, V>::operator=(HashMap&& other) noexcept
{
    assert(this != &other);

    Vector<iterator> new_index = std::move(other._index);
    LinkedList<value_type> new_data = std::move(other._data);

    _bucket_count = other._bucket_count;
    _bucket_mask = other._bucket_mask;
    assert(_isPowerOf2(_bucket_count));
    assert(_bucket_mask + 1 == _bucket_count);

    std::swap(_index, new_index);
    std::swap(_data, new_data);

    return *this;
}

template <typename K, typename V>
template <typename U>
typename std::enable_if<std::is_integral<U>::value, typename aisdi::HashMap<K, V>::size_type>::type
aisdi::HashMap<K, V>::_hashIndex(const U& key) const
{
    union { U integer; std::uint8_t bytes[sizeof(U)]; };
    integer = key;
    return (_hashBytes(bytes, sizeof(U)) & _bucket_mask);
}

template <typename K, typename V>
template <typename U>
typename std::enable_if<std::is_convertible<U, int>::value, typename aisdi::HashMap<K, V>::size_type>::type
aisdi::HashMap<K, V>::_hashIndex(int key) const
{
    union { int integer; std::uint8_t bytes[sizeof(int)]; };
    integer = key;
    return (_hashBytes(bytes, sizeof(int)) & _bucket_mask);
}

template <typename K, typename V>
template <typename U>
typename std::enable_if<std::is_base_of<std::string, U>::value, typename aisdi::HashMap<K, V>::size_type>::type
aisdi::HashMap<K, V>::_hashIndex(const U& key) const
{
    union { const char* chars; const std::uint8_t* bytes; };
    chars = key.c_str();
    return (_hashBytes(bytes, key.size()) & _bucket_mask);
}

template <typename K, typename V>
typename aisdi::HashMap<K, V>::size_type
aisdi::HashMap<K, V>::_hashBytes(const std::uint8_t* bytes, size_type count)
{
    // djb2 hash function
    size_type hash = 5381;
    for (size_type i = 0; i < count; ++i)
        hash = ((hash << 5) + hash) + bytes[i];

    return hash;
}

template <typename K, typename V>
bool
aisdi::HashMap<K, V>::_isPowerOf2(size_type v)
{
    return (v && ((v & (v - 1)) == 0));
}

template <typename K, typename V>
typename aisdi::HashMap<K, V>::size_type
aisdi::HashMap<K, V>::_ceilingPowerOf2(size_type v)
{
    // Find nearest power of two greater than size
    size_type power = 1;
    while (power < v) power <<= 1;
    assert(_isPowerOf2(power));

    return power;
}

template <typename K, typename V>
void
aisdi::HashMap<K, V>::_rehash()
{
    // Make a bigger HashMap (requested size will wrap up to the next power of two)
    HashMap <K, V> tmp(getSize() + 1);
    for (const auto& a : *this)
    {
        tmp.valueOf(a.first) = std::move(a.second);
    }

    std::swap(*this, tmp);
}

template <typename K, typename V>
bool
aisdi::HashMap<K, V>::isEmpty() const
{
    return (_data.getSize() == 0);
}

template <typename K, typename V>
typename aisdi::HashMap<K, V>::size_type
aisdi::HashMap<K, V>::getSize() const
{
    return _data.getSize();
}

template <typename K, typename V>
const typename aisdi::HashMap<K, V>::mapped_type&
aisdi::HashMap<K, V>::valueOf(const key_type& key) const
{
    const_iterator it = find(key);
    if (it == cend()) throw std::out_of_range("No such key");

    assert(it->first == key);
    return it->second;
}

template <typename K, typename V>
typename aisdi::HashMap<K, V>::mapped_type&
aisdi::HashMap<K, V>::valueOf(const key_type& key)
{
    if (_data.getSize() > _index.getSize()) _rehash();

    size_type idx = _hashIndex(key);
    typename Vector<iterator>::iterator it(_index.begin() + idx);

    if (*it == end())
    {
        // Bucket was empty
        value_type new_item(key, mapped_type{});

        // Navigate to next non-empty bucket (or end) and insert there
        typename Vector<iterator>::iterator tmp(it);
        while (tmp != _index.end() && *tmp == end()) ++tmp;

        typename LinkedList<value_type>::iterator s = _data.end();
        if (tmp != _index.end()) s = *tmp;

        _data.insert(s, new_item);
        *it = --s;
        assert(**it == new_item);
        return (*it)->second;
    }
    else
    {
        // Bucket was not empty
        iterator k = *it;
        while (k != end() && _hashIndex(k->first) == idx)
        {
            if (k->first == key) return k->second;
            ++k;
        }

        // Item has not been found, insert here
        value_type new_item(key, mapped_type());
        _data.insert(k, new_item);
        iterator tmp = --k;
        assert(*tmp == new_item);
        return tmp->second;
    }
}

template <typename K, typename V>
typename aisdi::HashMap<K, V>::const_iterator
aisdi::HashMap<K, V>::_find(const key_type& key) const
{
    // Get iterator from index
    size_type idx = _hashIndex(key);
    const_iterator it = *(_index.cbegin() + idx);

    // Look for matching key
    while (it != cend() && _hashIndex(it->first) == idx)
    {
        if (key == it->first) return it;
        ++it;
    }

    return cend();
}

template <typename K, typename V>
typename aisdi::HashMap<K, V>::const_iterator
aisdi::HashMap<K, V>::find(const key_type& key) const
{
    return _find(key);
}

template <typename K, typename V>
typename aisdi::HashMap<K, V>::iterator
aisdi::HashMap<K, V>::find(const key_type& key)
{
    ConstIterator it = _find(key);
    return Iterator(it);
}

template <typename K, typename V>
void
aisdi::HashMap<K, V>::remove(const key_type& key)
{
    remove(find(key));
}

template <typename K, typename V>
void
aisdi::HashMap<K, V>::remove(const const_iterator& pos)
{
    if (pos == cend()) throw std::out_of_range("No such item");

    // Check if pos is in index
    for (auto& a : _index)
    {
        if (a == pos)
        {
            // Set index entry to the next element or end()
            ConstIterator b = a + 1;
            if (b != end() && _hashIndex(b->first) == _hashIndex(a->first))
                a = b;
            else
                a = end();

            break;
        }
    }

    // Remove item
    _data.erase(pos);
}

template <typename K, typename V>
const typename aisdi::HashMap<K, V>::mapped_type&
aisdi::HashMap<K, V>::operator[](const key_type& key) const
{
    return valueOf(key);
}

template <typename K, typename V>
typename aisdi::HashMap<K, V>::mapped_type&
aisdi::HashMap<K, V>::operator[](const key_type& key)
{
    return valueOf(key);
}

template <typename K, typename V>
bool
aisdi::HashMap<K, V>::operator==(const HashMap& other) const
{
    if (getSize() != other.getSize()) return false;

    for (const auto& a : other)
    {
        const_iterator i = find(a.first);
        if (i == cend() || i->second != a.second) return false;
    }

    return true;
}

template <typename K, typename V>
bool
aisdi::HashMap<K, V>::operator!=(const HashMap& other) const
{
    return !(*this == other);
}

template <typename K, typename V>
typename aisdi::HashMap<K, V>::iterator
aisdi::HashMap<K, V>::begin()
{
    return Iterator(_data.begin());
}

template <typename K, typename V>
typename aisdi::HashMap<K, V>::iterator
aisdi::HashMap<K, V>::end()
{
    return Iterator(_data.end());
}

template <typename K, typename V>
typename aisdi::HashMap<K, V>::const_iterator
aisdi::HashMap<K, V>::cbegin() const
{
    return ConstIterator(_data.cbegin());
}

template <typename K, typename V>
typename aisdi::HashMap<K, V>::const_iterator
aisdi::HashMap<K, V>::cend() const
{
    return ConstIterator(_data.cend());
}

template <typename K, typename V>
typename aisdi::HashMap<K, V>::const_iterator
aisdi::HashMap<K, V>::begin() const
{
    return cbegin();
}

template <typename K, typename V>
typename aisdi::HashMap<K, V>::const_iterator
aisdi::HashMap<K, V>::end() const
{
    return cend();
}

#endif // !AISDI_HASHMAP_IMPL_HH
