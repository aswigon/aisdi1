#ifndef AISDI_HASHMAP_CONSTITERATOR_IMPL_HH
#define AISDI_HASHMAP_CONSTITERATOR_IMPL_HH

#ifndef AISDI_HASHMAP_H
#error "Only include HashMap.h"
#endif

#include "HashMap.h"

template <typename K, typename V>
typename aisdi::HashMap<K, V>::ConstIterator::pointer
aisdi::HashMap<K, V>::ConstIterator::operator->() const
{
    return (&this->operator*());
}

#endif // !AISDI_HASHMAP_CONSTITERATOR_IMPL_HH
