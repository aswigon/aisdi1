#ifndef AISDI_HASHMAP_ITERATOR_IMPL_HH
#define AISDI_HASHMAP_ITERATOR_IMPL_HH

#ifndef AISDI_HASHMAP_H
#error "Only include HashMap.h"
#endif

#include "HashMap.h"

template <typename K, typename V>
typename aisdi::HashMap<K, V>::Iterator::pointer
aisdi::HashMap<K, V>::Iterator::operator->() const
{
    return (&this->operator*());
}

template <typename K, typename V>
typename aisdi::HashMap<K, V>::Iterator::reference
aisdi::HashMap<K, V>::Iterator::operator*() const
{
    return const_cast<reference>(ConstIterator::operator*());
}

template <typename K, typename V>
typename aisdi::HashMap<K, V>::Iterator&
aisdi::HashMap<K, V>::Iterator::operator++()
{
    ConstIterator::operator++();
    return *this;
}

template <typename K, typename V>
typename aisdi::HashMap<K, V>::Iterator
aisdi::HashMap<K, V>::Iterator::operator++(int)
{
    auto tmp = *this;
    ConstIterator::operator++();
    return tmp;
}

template <typename K, typename V>
typename aisdi::HashMap<K, V>::Iterator&
aisdi::HashMap<K, V>::Iterator::operator--()
{
    ConstIterator::operator--();
    return *this;
}

template <typename K, typename V>
typename aisdi::HashMap<K, V>::Iterator
aisdi::HashMap<K, V>::Iterator::operator--(int)
{
    auto tmp = *this;
    ConstIterator::operator--();
    return tmp;
}

#endif // !AISDI_HASHMAP_ITERATOR_IMPL_HH
