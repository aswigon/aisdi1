#ifndef AISDI_LINKEDLIST_H
#define AISDI_LINKEDLIST_H

#include <cassert>
#include <cstddef>
#include <initializer_list>
#include <iterator>
#include <memory>
#include <stdexcept>
#include <type_traits>
#include <utility>

namespace aisdi
{
    template <typename T>
    class LinkedList
    {
    public:
        class ConstIterator;
        class Iterator;

        using difference_type = std::ptrdiff_t;
        using size_type = std::size_t;
        using value_type = T;
        using pointer = T*;
        using reference = T&;
        using const_pointer = const T*;
        using const_reference = const T&;
        using iterator = Iterator;
        using const_iterator = ConstIterator;
        using node_type = struct Node { Node* next; Node* prev; T data; };
        using node_pointer = Node*;

        LinkedList();
        LinkedList(std::initializer_list<T>);
        LinkedList(const LinkedList&);
        LinkedList(LinkedList&&) noexcept;
        ~LinkedList();

        LinkedList& operator=(const LinkedList&);
        LinkedList& operator=(LinkedList&&) noexcept;

        bool isEmpty() const;
        size_type getSize() const;
        void append(const_reference);
        void prepend(const_reference);
        void insert(const const_iterator&, const_reference);
        value_type popFirst();
        value_type popLast();
        void erase(const const_iterator&);
        void erase(const const_iterator&, const const_iterator&);

        iterator begin();
        iterator end();
        const_iterator cbegin() const;
        const_iterator cend() const;
        const_iterator begin() const;
        const_iterator end() const;

    private:
        std::allocator<Node> _allocator;
        size_type _size;
        node_pointer _head;
        node_pointer _tail;

        static typename std::enable_if<std::is_standard_layout<node_type>::value, node_pointer>::type
        _getNode(const_iterator);
    };

    template <typename T>
    class LinkedList<T>::ConstIterator
    {
    public:
        using iterator_category = std::bidirectional_iterator_tag;
        using value_type = typename LinkedList::value_type;
        using difference_type = typename LinkedList::difference_type;
        using pointer = typename LinkedList::const_pointer;
        using reference = typename LinkedList::const_reference;

        ConstIterator() = delete;
        ConstIterator(node_pointer);
        ConstIterator(const ConstIterator&) = default;

        reference operator*() const;
        ConstIterator& operator++();
        ConstIterator operator++(int);
        ConstIterator& operator--();
        ConstIterator operator--(int);
        ConstIterator operator+(difference_type) const;
        ConstIterator operator-(difference_type) const;
        bool operator==(const ConstIterator&) const;
        bool operator!=(const ConstIterator&) const;

    private:
        node_pointer _pos;
    };

    template <typename T>
    class LinkedList<T>::Iterator : public LinkedList<T>::ConstIterator
    {
    public:
        using pointer = typename LinkedList::pointer;
        using reference = typename LinkedList::reference;

        Iterator() = delete;
        Iterator(node_pointer ptr) : ConstIterator(ptr) {}
        Iterator(const ConstIterator& other) : ConstIterator(other) {}

        reference operator*() const;
        Iterator& operator++();
        Iterator operator++(int);
        Iterator& operator--();
        Iterator operator--(int);
        Iterator operator+(difference_type) const;
        Iterator operator-(difference_type) const;
    };
}

#include "LinkedList/LinkedList_impl.hh"
#include "LinkedList/LinkedList_ConstIterator_impl.hh"
#include "LinkedList/LinkedList_Iterator_impl.hh"

#endif // !AISDI_LINKEDLIST_H
