#ifndef AISDI_HASHMAP_H
#define AISDI_HASHMAP_H

#include <cassert>
#include <cstddef>
#include <cstdint>
#include <initializer_list>
#include <iterator>
#include <stdexcept>
#include <string>
#include <type_traits>
#include <utility>

#include "LinkedList.h"
#include "Vector.h"

namespace aisdi
{
    template <typename K, typename V>
    class HashMap
    {
    public:
        class ConstIterator;
        class Iterator;

        using key_type = K;
        using mapped_type = V;
        using value_type = std::pair<const key_type, mapped_type>;
        using size_type = std::size_t;
        using reference = value_type&;
        using const_reference = const value_type&;
        using iterator = Iterator;
        using const_iterator = ConstIterator;

        HashMap() : HashMap(_default_bucket_count) {}
        HashMap(size_type);
        HashMap(std::initializer_list<value_type>);
        HashMap(const HashMap&);
        HashMap(HashMap&&) noexcept;
        ~HashMap() = default;

        HashMap& operator=(const HashMap&);
        HashMap& operator=(HashMap&&) noexcept;

        bool isEmpty() const;
        size_type getSize() const;
        const mapped_type& valueOf(const key_type&) const;
        mapped_type& valueOf(const key_type&);
        const_iterator find(const key_type&) const;
        iterator find(const key_type&);
        void remove(const key_type&);
        void remove(const const_iterator&);

        const mapped_type& operator[](const key_type&) const;
        mapped_type& operator[](const key_type&);
        bool operator==(const HashMap&) const;
        bool operator!=(const HashMap&) const;

        iterator begin();
        iterator end();
        const_iterator cbegin() const;
        const_iterator cend() const;
        const_iterator begin() const;
        const_iterator end() const;

    private:
        static const size_type _default_bucket_count = (1 << 5); // 32
        size_type _bucket_count = _default_bucket_count;
        size_type _bucket_mask = _default_bucket_count - 1;
        Vector<iterator> _index;
        LinkedList<value_type> _data;

        template <typename U = K>
        typename std::enable_if<std::is_integral<U>::value, size_type>::type
        _hashIndex(const U&) const;

        template <typename U = K>
        typename std::enable_if<std::is_convertible<U, int>::value, size_type>::type
        _hashIndex(int) const;

        template <typename U = K>
        typename std::enable_if<std::is_base_of<std::string, U>::value, size_type>::type
        _hashIndex(const U&) const;

        static size_type _hashBytes(const std::uint8_t*, size_type);
        static bool _isPowerOf2(size_type);
        static size_type _ceilingPowerOf2(size_type);

        void _rehash();
        const_iterator _find(const key_type&) const;

#if defined(DEBUG) && defined(AISDI_ENABLE_OSTREAM)
        friend std::ostream& operator<<<K, V>(std::ostream&, const HashMap<K, V>&);
#endif
    };

    template <typename K, typename V>
    class HashMap<K, V>::ConstIterator : public LinkedList<value_type>::ConstIterator
    {
    public:
        using reference = typename HashMap::const_reference;
        using iterator_category = std::bidirectional_iterator_tag;
        using value_type = typename HashMap::value_type;
        using pointer = const typename HashMap::value_type*;

        ConstIterator() = delete;
        ConstIterator(const ConstIterator& other) : LinkedList<value_type>::ConstIterator(other) {}
        ConstIterator(const typename LinkedList<value_type>::ConstIterator& other)
            : LinkedList<value_type>::ConstIterator(other) {}

        pointer operator->() const;
    };

    template <typename K, typename V>
    class HashMap<K, V>::Iterator : public HashMap<K, V>::ConstIterator
    {
    public:
        using reference = typename HashMap::reference;
        using pointer = typename HashMap::value_type*;

        Iterator() = delete;
        Iterator(const ConstIterator& other) : ConstIterator(other) {}
        Iterator(const typename LinkedList<value_type>::Iterator& other) : ConstIterator(other) {}

        pointer operator->() const;
        reference operator*() const;
        Iterator& operator++();
        Iterator operator++(int);
        Iterator& operator--();
        Iterator operator--(int);
    };
}

#include "HashMap/HashMap_impl.hh"
#include "HashMap/HashMap_ConstIterator_impl.hh"
#include "HashMap/HashMap_Iterator_impl.hh"

#endif // !AISDI_HASHMAP_H
