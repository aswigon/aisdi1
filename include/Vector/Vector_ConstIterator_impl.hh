#ifndef AISDI_VECTOR_CONSTITERATOR_IMPL_HH
#define AISDI_VECTOR_CONSTITERATOR_IMPL_HH

#ifndef AISDI_VECTOR_H
#error "Include only Vector.h"
#endif

#include "Vector.h"

template <typename T>
aisdi::Vector<T>::ConstIterator::ConstIterator(const_pointer const* b, const size_type* s, size_type d)
{
    _size = s;
    _buf = b;
    _pos = d;

    if (_pos > *_size) throw std::out_of_range("Iterator out of range");
}

template <typename T>
typename aisdi::Vector<T>::const_reference
aisdi::Vector<T>::ConstIterator::operator*() const
{
    if (_pos >= *_size) throw std::out_of_range("Iterator out of range");
    return (*_buf)[_pos];
}

template <typename T>
typename aisdi::Vector<T>::ConstIterator&
aisdi::Vector<T>::ConstIterator::operator++()
{
    if (_pos >= *_size) throw std::out_of_range("Iterator out of range");
    ++_pos;
    return *this;
}

template <typename T>
typename aisdi::Vector<T>::ConstIterator
aisdi::Vector<T>::ConstIterator::operator++(int)
{
    auto tmp = *this;
    operator++();
    return tmp;
}

template <typename T>
typename aisdi::Vector<T>::ConstIterator&
aisdi::Vector<T>::ConstIterator::operator--()
{
    if (_pos == 0) throw std::out_of_range("Iterator out of range");
    --_pos;
    return *this;
}

template <typename T>
typename aisdi::Vector<T>::ConstIterator
aisdi::Vector<T>::ConstIterator::operator--(int)
{
    auto tmp = *this;
    operator--();
    return tmp;
}

template <typename T>
typename aisdi::Vector<T>::ConstIterator
aisdi::Vector<T>::ConstIterator::operator+(difference_type d) const
{
    ConstIterator tmp(_buf, _size, _pos + d);
    return tmp;
}

template <typename T>
typename aisdi::Vector<T>::ConstIterator
aisdi::Vector<T>::ConstIterator::operator-(difference_type d) const
{
    return operator+(-d);
}

template <typename T>
bool
aisdi::Vector<T>::ConstIterator::operator==(const ConstIterator& other) const
{
    return (_buf == other._buf && _pos == other._pos);
}

template <typename T>
bool
aisdi::Vector<T>::ConstIterator::operator!=(const ConstIterator& other) const
{
    return !(*this == other);
}

#endif // !AISDI_VECTOR_CONSTITERATOR_IMPL_HH
