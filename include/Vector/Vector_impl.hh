#ifndef AISDI_VECTOR_IMPL_HH
#define AISDI_VECTOR_IMPL_HH

#ifndef AISDI_VECTOR_H
#error "Include only Vector.h"
#endif

#include "Vector.h"

// Guarantee for HashMap: Vector doubles in size when growing

template <typename T>
aisdi::Vector<T>::Vector(size_type count)
{
    if (count >= _max_count) throw std::overflow_error("Overflow");

    _buffer = nullptr;
    _size = 0;
    _capacity = count;

    // Allocate memory only if non-zero capacity has been requested
    if (count > 0) _buffer = _allocator.allocate(count);
}

template <typename T>
aisdi::Vector<T>::Vector(std::initializer_list<T> list) : Vector(list.size())
{
    for (auto& a : list) append(a);
}

template <typename T>
aisdi::Vector<T>::Vector(const Vector& other) : Vector(other.getSize())
{
    _allocator = other._allocator;
    _size = other.getSize();
    std::copy(other.begin(), other.end(), begin());
}

template <typename T>
aisdi::Vector<T>::Vector(Vector&& other) noexcept
{
    _allocator = std::move(other._allocator);
    _size = other._size;
    _capacity = other._capacity;
    _buffer = other._buffer;
    other._size = 0;
    other._capacity = 0;
    other._buffer = nullptr;
}

template <typename T>
aisdi::Vector<T>::~Vector()
{
    erase(begin(), end());
    if (_buffer != nullptr) _allocator.deallocate(_buffer, _capacity);
}

template <typename T>
typename aisdi::Vector<T>&
aisdi::Vector<T>::operator=(const Vector& other)
{
    if (this == &other) return *this;

    erase(begin(), end());
    _allocator.deallocate(_buffer, _capacity);
    _buffer = nullptr;
    _capacity = 0;
    _allocator = other._allocator;
    for (auto& a : other) append(a);
    return *this;
}

template <typename T>
typename aisdi::Vector<T>&
aisdi::Vector<T>::operator=(Vector&& other) noexcept
{
    assert(this != &other);

    erase(begin(), end());
    _allocator.deallocate(_buffer, _capacity);
    _allocator = std::move(other._allocator);
    _size = other._size;
    _capacity = other._capacity;
    _buffer = other._buffer;
    other._size = 0;
    other._capacity = 0;
    other._buffer = nullptr;
    return *this;
}

template <typename T>
void
aisdi::Vector<T>::_grow()
{
    // Container could be empty
    if (_capacity == 0)
    {
        assert(_buffer == nullptr);
        _capacity = _default_nonzero_count;
        _buffer = _allocator.allocate(_capacity);
    }
    else if (_capacity <= (_max_count / 2))
    {
        assert(_buffer != nullptr);
        size_type new_capacity = 2 * _capacity;
        value_type* new_buffer = _allocator.allocate(new_capacity);

        Iterator tmp_iter(&new_buffer, &new_capacity, 0);
        std::move(begin(), end(), tmp_iter);

        _allocator.deallocate(_buffer, _capacity);
        _capacity = new_capacity;
        _buffer = new_buffer;
    }
    else
    {
        throw std::overflow_error("Overflow");
    }
}

template <typename T>
bool
aisdi::Vector<T>::isEmpty() const
{
    return (_size == 0);
}

template <typename T>
typename aisdi::Vector<T>::size_type
aisdi::Vector<T>::getSize() const
{
    return _size;
}

template <typename T>
void
aisdi::Vector<T>::append(const_reference item)
{
    if (_size >= _capacity) _grow();
    Iterator i = end();
    ++_size;
    new(&*i) T(item);
}

template <typename T>
void
aisdi::Vector<T>::prepend(const_reference item)
{
    insert(begin(), item);
}

template <typename T>
void
aisdi::Vector<T>::insert(const const_iterator& pos, const_reference item)
{
    if (_size >= _capacity) _grow();
    Iterator i = pos;
    ++_size;

    // Move (part of) memory to the right
    std::move_backward(i, end() - 1, end());
    new(&*i) T(item);
}

template <typename T>
typename aisdi::Vector<T>::value_type
aisdi::Vector<T>::popFirst()
{
    if (_size == 0) throw std::logic_error("Cannot pop from empty collection");

    value_type tmp = std::move(_buffer[0]);
    // Move memory to the left
    std::move(begin() + 1, end(), begin());
    --_size;
    return tmp;
}

template <typename T>
typename aisdi::Vector<T>::value_type
aisdi::Vector<T>::popLast()
{
    if (_size == 0) throw std::logic_error("Cannot pop from empty collection");

    return std::move(_buffer[--_size]);
}

template <typename T>
void
aisdi::Vector<T>::erase(const const_iterator& pos)
{
    Iterator i = pos;
    // Destruct item
    (&*i)->~T();
    // Move memory to the left
    std::move(i + 1, end(), i);
    --_size;
}

template <typename T>
void
aisdi::Vector<T>::erase(const const_iterator& left, const const_iterator& right)
{
    size_type new_size = _size;
    // Destruct items
    for (Iterator i = left; i != right; ++i)
    {
        (&*i)->~T();
        --new_size;
    }

    Iterator l = left;
    Iterator r = right;
    // Move memory to the left
    std::move(r, end(), l);
    _size = new_size;
}

template <typename T>
typename aisdi::Vector<T>::iterator
aisdi::Vector<T>::begin()
{
    return Iterator(&_buffer, &_size, 0);
}

template <typename T>
typename aisdi::Vector<T>::iterator
aisdi::Vector<T>::end()
{
    return Iterator(&_buffer, &_size, _size);
}

template <typename T>
typename aisdi::Vector<T>::const_iterator
aisdi::Vector<T>::cbegin() const
{
    return ConstIterator(&_buffer, &_size, 0);
}

template <typename T>
typename aisdi::Vector<T>::const_iterator
aisdi::Vector<T>::cend() const
{
    return ConstIterator(&_buffer, &_size, _size);
}

template <typename T>
typename aisdi::Vector<T>::const_iterator
aisdi::Vector<T>::begin() const
{
    return cbegin();
}

template <typename T>
typename aisdi::Vector<T>::const_iterator
aisdi::Vector<T>::end() const
{
    return cend();
}

#endif // !AISDI_VECTOR_IMPL_HH
