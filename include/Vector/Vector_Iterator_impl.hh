#ifndef AISDI_VECTOR_ITERATOR_IMPL_HH
#define AISDI_VECTOR_ITERATOR_IMPL_HH

#ifndef AISDI_VECTOR_H
#error "Include only Vector.h"
#endif

#include "Vector.h"

template <typename T>
typename aisdi::Vector<T>::reference
aisdi::Vector<T>::Iterator::operator*() const
{
    return const_cast<reference>(ConstIterator::operator*());
}

template <typename T>
typename aisdi::Vector<T>::Iterator&
aisdi::Vector<T>::Iterator::operator++()
{
    ConstIterator::operator++();
    return *this;
}

template <typename T>
typename aisdi::Vector<T>::Iterator
aisdi::Vector<T>::Iterator::operator++(int)
{
    auto tmp = *this;
    ConstIterator::operator++();
    return tmp;
}

template <typename T>
typename aisdi::Vector<T>::Iterator&
aisdi::Vector<T>::Iterator::operator--()
{
    ConstIterator::operator--();
    return *this;
}

template <typename T>
typename aisdi::Vector<T>::Iterator
aisdi::Vector<T>::Iterator::operator--(int)
{
    auto tmp = *this;
    ConstIterator::operator--();
    return tmp;
}

template <typename T>
typename aisdi::Vector<T>::Iterator
aisdi::Vector<T>::Iterator::operator+(difference_type d) const
{
    return ConstIterator::operator+(d);
}

template <typename T>
typename aisdi::Vector<T>::Iterator
aisdi::Vector<T>::Iterator::operator-(difference_type d) const
{
    return ConstIterator::operator-(d);
}

#endif // !AISDI_VECTOR_ITERATOR_IMPL_HH
